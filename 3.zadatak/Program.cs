﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataset virtualDataset = new VirtualProxyDataset("C:\\Users\\Korisnik\\source\\repos\\ConsoleApp24\\ConsoleApp24\\bin\\Debug\\LV5.txt");
            User myUser = User.GenerateUser("User");
            IDataset protectionDataset = new ProtectionProxyDataset(myUser);

            DataConsolePrinter printToConsole = new DataConsolePrinter();

            Console.WriteLine("2.2. test: ");
            printToConsole.Print(virtualDataset);
            Console.WriteLine("2.3. test: ");
            printToConsole.Print(protectionDataset);
        }
    }
}
