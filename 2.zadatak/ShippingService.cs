﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class ShippingService
    {
        private double UnitPrice;
        public double ShippingPriceOfPackage(IShipable element)
        {
            double totalPrice = 0.0;
            totalPrice = UnitPrice * element.Weight;
            return totalPrice;
        }

        public ShippingService(double UnitPrice)
        {
            this.UnitPrice = UnitPrice;
        }
    }
}
