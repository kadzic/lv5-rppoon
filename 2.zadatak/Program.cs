﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> shipables = new List<IShipable>();

            Box student_box = new Box("Student box");

            Product soup = new Product("Soup", 2.50, 0.05);
            Product chocolateBar = new Product("Snickers", 2.00, 0.20);
            Product chewingGum = new Product("Gum", 0.50, 0.03);

            student_box.Add(soup);
            student_box.Add(chocolateBar);
            student_box.Add(chewingGum);

            Console.WriteLine("Total price in the box: ");
            Console.WriteLine(Math.Round(student_box.Price, 2));
            Console.WriteLine("Total weight in the box: ");
            Console.WriteLine(Math.Round(student_box.Weight, 2));

            ShippingService shipping = new ShippingService(2.00);

            Console.WriteLine("Shipping price: ");
            Console.WriteLine(shipping.ShippingPriceOfPackage(student_box));
        }
    }
}
