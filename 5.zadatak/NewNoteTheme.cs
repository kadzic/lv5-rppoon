﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp25
{
    class NewNoteTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
        }

        public string GetHeader(int width)
        {
            return new string('!', width);
        }

        public string GetFooter(int width)
        {
            return new string('?', width);
        }
    }
}
