﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp25
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new LightTheme();
            Note note = new ReminderNote("RPPOON - LV5", theme);

            note.Show();

            ITheme theme1 = new NewNoteTheme();
            note.Theme = theme1;

            note.Show();

            ITheme theme2 = new NewNoteTheme();

            Note note1 = new ReminderNote("Hello", theme);
            note1.Theme = theme2;

            note1.Show();
        }
    }
}
