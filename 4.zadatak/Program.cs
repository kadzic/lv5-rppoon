﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataset proxyLogger = new ProxyLogger("C:\\Users\\Korisnik\\source\\repos\\ConsoleApp24\\ConsoleApp24\\bin\\Debug\\LV5.txt");
            DataConsolePrinter PrintToConsole = new DataConsolePrinter();

            ReadOnlyCollection<List<string>> elements = proxyLogger.GetData();

            PrintToConsole.Print(proxyLogger);

        }
    }
}
