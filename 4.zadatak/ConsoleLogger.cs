﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        
        private ConsoleLogger() { }

        public static ConsoleLogger GetInstance()
        {
            if(instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }

        public string LogIn()
        {
            string log = "Logged at: " + DateTime.Now.ToString();
            return log;
        }
    }
}
