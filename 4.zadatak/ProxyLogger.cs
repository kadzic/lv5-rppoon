﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class ProxyLogger : IDataset
    {
        private Dataset dataset;
        private string filePath;

        public ProxyLogger(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            Console.WriteLine(ConsoleLogger.GetInstance().LogIn());
            return dataset.GetData();
        }
    }
}
