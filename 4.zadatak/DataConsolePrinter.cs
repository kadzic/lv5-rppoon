﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp24
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void Print(IDataset dataset)
        {
            foreach(List<string> item in dataset.GetData())
            {
                foreach(string element in item)
                {
                    Console.WriteLine(element);
                }
            }
        }
    }
}
